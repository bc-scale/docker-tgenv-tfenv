# Docker - tgenv & tfenv
[**Docker Hub Image »**](https://hub.docker.com/r/jarylc/tgenv-tfenv-alpine)

## About
- This repository keeps an Alpine Linux Docker image up to date with the latest [tgenv](https://github.com/cunymatthieu/tgenv) and [tfenv](https://github.com/tfutils/tfenv).
- This repository should be mainly used for CI/CD.

## Usage
### CI/CD
Remember to add a call to `source envs` or `. envs` before running any commands to set proper paths.

Gitlab example:
```yaml
image:
  name: minimages/tgenv-tfenv-alpine
stages:
  - build

build:
  stage: build
  before_script:
    - source envs
    - tgenv install && tfenv install
  script:
    - terragrunt -version && terraform -version
```
